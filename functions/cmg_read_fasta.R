#----------------------------------------------------
cmg_read_fasta <- function(filename, type, orgname) {
#----------------------------------------------------
    # contigs <- cmg_read_fasta("contigs_LGYT01_1.fasta", type = 'nuc', orgname = 'LGYT01_1')

    if (!type %in% c('aa', 'nuc')) {
        print ("# ERROR: type not recognized, must be aa (amino acids) or nuc (nucleotides)")
        return()
    }

    fastaObj <- data.frame( orgname= character(), seq=character(), name=character() , length=integer(), type =character(), stringsAsFactors = FALSE)
    fasta <- readLines(filename)
    headerIndex <- grep(">", fasta)

    for (i in seq(1, length(headerIndex)) ) {
        index <- headerIndex[i]
        indexNext <- length(fasta)
        if ( i != length(headerIndex) ) {
            indexNext <- headerIndex[i+1]-1
        }

        iname <- fasta[index]
        indexSeq <- seq(index+1,indexNext)
        iseq <- fasta[min(indexSeq):max(indexSeq)]
        iseq <- paste(iseq, collapse = '')
        ifeatureid <- lapply(strsplit(iname, '\\|'), `[[`, 3)[[1]]
        ilength <- length(unlist( strsplit(iseq, '') ))
        if (ilength > 0) {
            fastaObj <- rbind(fastaObj, data.frame( orgname = orgname, seq=iseq, name=iname, length=ilength, featureid = ifeatureid, type = type, stringsAsFactors = FALSE ) )
        }
        else {
            print(paste( "# WARNING! Found sequence of length 0 - ", iname, "#") )
        }
    }
    return ( fastaObj )
}



#----------------------------------------------------
cmg_read_fasta_assembly <- function(filename, type, orgname) {
#----------------------------------------------------
    # contigs <- cmg_read_fasta("contigs_LGYT01_1.fasta", type = 'nuc', orgname = 'LGYT01_1')

    if (!type %in% c('aa', 'nuc')) {
        print ("# ERROR: type not recognized, must be aa (amino acids) or nuc (nucleotides)")
        return()
    }

    fastaObj <- data.frame( orgname= character(), seq=character(), name=character() , length=integer(), type =character(), stringsAsFactors = FALSE)
    fasta <- readLines(filename)
    headerIndex <- grep(">", fasta)

    for (i in seq(1, length(headerIndex)) ) {
        index <- headerIndex[i]
        indexNext <- length(fasta)
        if ( i != length(headerIndex) ) {
            indexNext <- headerIndex[i+1]-1
        }

        iname <- fasta[index]
        indexSeq <- seq(index+1,indexNext)
        iseq <- fasta[min(indexSeq):max(indexSeq)]
        iseq <- paste(iseq, collapse = '')
        ilength <- length(unlist( strsplit(iseq, '') ))
        if (ilength > 0) {
            fastaObj <- rbind(fastaObj, data.frame( orgname = orgname, seq=iseq, name=iname, length=ilength, type = type, stringsAsFactors = FALSE ) )
        }
        else {
            print(paste( "# WARNING! Found sequence of length 0 - ", iname, "#") )
        }
    }
    return ( fastaObj )
}
