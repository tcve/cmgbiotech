setwd("~/Dropbox/PhD/TAing:Supervising/27421 Comparative Microbial Genomics in Biotechnology/Ex_1_intro_to_R")
getwd()


## Data Structures/Objects Questions

#2 Set the current working directory to the folder where script is saved
# setwd("~/Dropbox/PhD/TAing:Supervising/27421 Comparative Microbial Genomics in Biotechnology/Ex_1_intro_to_R")
#3 Create a numeric vector in the Console using the c() function
c(1, 2.5, 4.5)
#4 Save the vector in a variable
vector <-c(1, 2.5, 4.5)


## Data operations Questions

#2 Find the help page for the function write.table, use the Help tab or the question mark function ?write.table
?write.table
#3 Save the mtcars data in a file using write.table
write.table(mtcars, file = "mtcars")
#7 Find the help page for the function read.table
?read.table
#8 Load the mtcars data into a variable (data) from the file using read.table
data<-read.table("mtcars")
#10 What type of object is data?
str(data) # data frame


## Data interaction Questions

#1 What does the command data[1,3] return?
data[1,3] #160
#2 How can you access the value of cyl for the Merc 450SL?
data["Merc 450SL", "cyl"] #yes
#3 How do you get the entire third column of data?
data[3]
#4 What does the data$cyl return?
data$cyl # all the corresponding values in order of appearance
#5 Is data$cyl[1] the same as data[1,2]? Why, why not?
data$cyl[1]
data[1,2]
#6 What does the functions rownames and colnames return?
rownames(data) 
colnames(data)


## Type conversion Questions

#1 Is the object data$cyl a vector?
is.vector(data$cyl) #TRUE
#2 Is the object data a matrix?
is.matrix(data) #FALSE
#3 Create a matrix object named datamatrix from the data object
datamatrix<-as.matrix(data)
#4 Can you access datamatrix$cyl like you could with data$cyl? Why, why not?
datamatrix["Merc 450SL", "cyl"]
#5 What is the sum of values in the data$disp column (sum() function)?
sum(data$disp) #7383.1
#6 Create a character vector from the data$disp data
dispvector<-as.vector(data$disp)
#7 What is the sum of the character vector?
sum(dispvector) #7383.1


##Data in action Questions 1

#1 Copy and paste the two vectors below into your R script and run the lines
organisms_names<-c("Aspergillus aculeatinus","Aspergillus brunneoviolaceus", "Aspergillus campestris", "Aspergillus costaricaensis",
                                "Aspergillus ellipticus", "Aspergillus eucalypticola", "Aspergillus fijiensis",
                                "Aspergillus heteromorphus", "Aspergillus homomorphus", "Aspergillus ibericus",
                                "Aspergillus indologenus CBS 114.80", "Aspergillus japonicus",
                                "Aspergillus lacticoffeatus", "Aspergillus neoniger", "Aspergillus novofumigatus",
                                "Aspergillus ochraceoroseus", "Aspergillus piperis", "Aspergillus saccharolyticus",
                                "Aspergillus sclerotiicarbonarius", "Aspergillus sclerotioniger",
                                "Aspergillus steynii", "Aspergillus uvarum", "Aspergillus vadensis",
                                "Aspergillus violaceofuscus")
total_genes<-c(12082L, 12132L, 12017L, 13211L, 12338L, 12571L, 10066L, 12069L,
                            8924L, 11549L, 11939L, 13083L, 12024L, 12163L, 11680L, 11361L,
                            11133L, 12019L, 11934L, 12884L, 11966L, 9764L, 12075L, 12027L)
#2 Which class of data are these two vectors?
str(total_genes) #integer
str(organisms_names) #character
#3 Use the length function to find the number of components in each vector
length(total_genes) #24
length(organisms_names) #24


##Data in action Questions 2

#1 What is the result of using the plot function on the vector total_genes?
plot(total_genes)
#3 What does the function boxplot do?
boxplot(total_genes)
#4 What is the product of the command data.frame(y=organisms_names, x=total_genes)?
data.frame(y=organisms_names, x=total_genes)
#5 What is the difference between the product above and the one produced by this command data.frame(organisms_names=organisms_names, total_genes=total_genes)?
data.frame(organisms_names=organisms_names, total_genes=total_genes) # changes/specifies the names of the variables y and x
#6 Save the above data object in a variable called organism_dataframe
organisms_dataframe<-data.frame(organisms_names=organisms_names, total_genes=total_genes)


## Data in action Questions 3

#1 Install the package ggplot2 using the Packages tab or the function install.packages('ggplot2') in the Console.
#1 Note the quotation marks when referring to the package name in the install.packages function.
# install.packages('ggplot2')
#2 Load the package using the Packages tab or the function library('ggplot2') in the Console
library("ggplot2")


## Data in action Questions 4
filepath<- "https://bitbucket.org/tcve/cmgbiotech/raw/ab6c584b011fe2a9a4ea5225ea241dc5957d71d2/data/geneCounts.txt"
myData<- read.table( file=url(filepath), header=T, sep="\t")

#1 What does the sep argument do? What happens if you set it to sep='\s'?
myData2<- read.table( file=url(filepath), header=T, sep="\s") #Error: '\s' is an unrecognized escape in character string starting ""\s"
#1 What does the sep argument do? What happens if you set it to sep=';'?
myData2<- read.table( file=url(filepath), header=T, sep=';') # Wrong import
#2 Describe the myData structure
str(myData) #data frame


## Data in action Questions 5
qplot( data=myData, x=total_genes, main="Histogram of gene counts")
ggplot(myData, aes(x=total_genes)) + geom_histogram() #no title, +makes histogram
ggplot(myData, aes(x=total_genes, fill=section)) + geom_histogram() #colors columns according to section
ggplot(myData, aes(x=total_genes, y=section)) + geom_point() #section as 'y'
d<- ggplot(myData, aes(x=total_genes, y=section))
d + geom_point()
d + geom_point(aes(colour = section)) #colours according to section
d + geom_point(aes(colour = section)) + scale_colour_brewer() # gradient from colour_brewer

#6 Can you make a histogram of x=section?
ggplot(myData, aes(y=total_genes, x=section)) + geom_point() #section as 'x'
#7 Make a box and whiskers plot of x=total_genes with a box for each section.
boxplot(myData, aes(x=total_genes, y=section))
#7 The function is called geom_boxplot(). Find examples on the ggplot help pages.
ggplot(data=myData, aes(y=section, x=total_genes)) + geom_boxplot(position='dodge')