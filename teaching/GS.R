
#2) Set the filepath and load the FASTA file into a data frame using the <code>url</code> function, see below.
filepath <- "https://bitbucket.org/tcve/cmgbiotech/raw/f37461c1f92cb29bd7fa53867ed910d8e0809041/data/Aspfu1_GeneCatalog_transcripts_20120808.nt.fasta"
myData <- read.table( file=url(filepath), header=T, sep="\t")

source('cmg_read_fasta.R')

## Data construction

## Data exploration

### Protein lengths and patterns

Aspbr1_aa <- cmg_read_fasta(
filename = '/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/data/Aspbr1/Aspbr1_GeneCatalog_proteins_20120615.aa.fasta', 
type = 'aa', orgname = 'Aspbr1')

Aspcos1_aa <- cmg_read_fasta(
filename = '/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/data/Aspcos1/Aspcos1_GeneCatalog_proteins_20141208.aa.fasta', 
type = 'aa', orgname = 'Aspcos1')

Aspacu1_aa <- cmg_read_fasta(
filename = '/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/data/Aspacu1/Aspacu1_GeneCatalog_proteins_20140714.aa.fasta', 
type = 'aa', orgname = 'Aspacu1')

Alta1_aa <- cmg_read_fasta(
filename = '/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/data/Altal1/Altal1_GeneCatalog_CDS_20141010.fasta', 
type = 'aa', orgname = 'Alta1')

Aspfo1_aa <- cmg_read_fasta(
filename = '/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/data/Aspfo1/Aspfo1_GeneCatalog_proteins_20120615.aa.fasta', 
type = 'aa', orgname = 'Aspfo1')

allFSAaa <- rbind(Aspbr1_aa, Aspcos1_aa, Alta1_aa, Aspacu1_aa, Aspfo1_aa)
unique(allFSAaa$orgname)


#6) Make a histogram, of protein lengths for one of the organisms using the <code>qplot()</code> or <code>gplot()</code> functions. Save the plot.</div>


# 1) Combine/bind all protein FASTA variables into one, call it <code>allFSA</code>.

#5) Use <code>ggplot</code> to create a histogram of protein lengths for all data points, do not separate by organism. Save the plot.

# Format of command to plot using ggplot
ggplot(data=dataframe, aes(x=value)) + geom_histogram()

# 6) Use <code>ggplot</code> to create a histogram of protein lengths separating each organism, plotting in one graph (hint, use the <code>fill</code> argument, it is an aesthetics argument). Save the plot.

#7) The <code>geom_histogram()</code> function has a number of detail control arguments. Use the <code>Help</code> pages to investigate the arguments. What is controlled by setting the <code>bins</code> argument?

#9) Use the <code>geom_boxplot()</code> function to investigate the maximum, average and minimum lengths of proteins for different organisms. Is there more similarity between Aspergilli than between Aspergilli and the additional species? Save the plot.


## Data experimentation

### Amino acid usage

source('/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/functions/cmg_lookups.R')
#3) Call the <code>cmg_aminoAcids()</code> function without arguments and store in a variable called <code>aminoAcids</code>. What is the structure of this variable?

source('/Users/tammi/Dropbox/Drop-WORK/bitbucket/cmgbiotech/functions/cmg_count_pattern.R')

# Example 1
for (year in c(2010,2011,2012,2013,2014,2015) ) {
  print( paste( "The year is", year ) )
}

# Example 2
years <- c(2010,2011,2012,2013,2014,2015)
for ( year in years ) {
  print( paste( "The year is", year ) )
}
# Example 3
for ( i in seq(1, length( years) ) ) {
  print( paste( "The year is", years[i] ) )
}

class(years)


#7) Create a <code>for</code> loop that runs through the <code>aminoAcids$code1</code> vector using the vector index (use the <code>seq()</code> function). Print out the index value (<code>i</code>) and the corresponding of <code>aminoAcids$code1</code> value.
#8) Make a copy of <code>allFSA</code> and name it <code>allFSA_count</code>. Use the <code><-</code> operator like when assigning a value to a variable.
#9) Create a <code>for</code> loop that runs through the <code>aminoAcids$code1</code> vector using the value of <code>aminoAcids$code1</code>. Print out this value as the loop runs. Within the loop, add the call to the <code>cmg_count_pattern()</code>. The <code>pattern</code> should be set to the value of <code>aminoAcids$code1</code>, the <code>dataframe</code> should be <code>allFSA_count</code>, the column to be search is the column of <code>allFSA_count</code> containing the sequence and the <code>type</code> is amino acids. For each loop, make sure to store the data in <code>allFSA_count</code>. 

# Example with cmg_nucleotides
nuc <- cmg_nucleotides()
FSAcopy <- allFSA

for (n in nuc$code1) {
    FSAcopy <- cmg_count_pattern( 
        pattern = n, dataframe = FSAcopy, column = 'seq', type = 'nuc')
}

#10) Make a histogram using <code>ggplot()</code>. Set the <code>x</code> value to one of the counts or percentages and use the <code>fill</code> argument to make a bar for each organism. Try using the <code>position = 'dodge'</code> argument for the <code>geom_histogram()</code> function. Save the plots.

# [code lang="r"]
# > str(allFSA_count)
# 'data.frame':	25027 obs. of  29 variables:
#  $ orgname   : chr  "Aspbr1" "Aspbr1" "Aspbr1" "Aspbr1" ...
#  $ seq       : chr  "MSSFWNPNNNPTSSSNHRRRRIKGKQRANAM...
#  $ name      : chr  ">jgi|Aspbr1|24568|fgenesh1_pg.1_#_100"...
#  $ length    : int  122 280 174 1220 153 896 865 263 252 320 ...
#  $ type      : chr  "aa" "aa" "aa" "aa" ...
# 
#  ..........

# Histogram of a specific count using `ggplot`
ggplot(data=allFSA_count, aes(x=count_W_aa, fill=orgname)) +
geom_histogram(position = 'dodge')


## Heatmaps

organisms <- c('orgA', 'orgB', 'orgC', 'orgD')
measure1 <- c(1,2,1,2)
measure2 <- c(4,3,5,6)
measure3 <- c(2,1,1,1)
measure4 <- c(6,5,6,5)
measureData <- data.frame(organisms, measure1, measure2, measure3, measure4)


matrixData <- measureData[,2:ncol(measureData)]
matrix <- data.matrix(matrixData) > rownames(matrix)
#NULL
colnames(matrix)
#[1] "measure1" "measure2" "measure3" "measure4"

rownames(matrix) <- measureData$organisms

# Using build-in heatmap
heatmap(matrix)
heatmap(matrix, margins = c(10,10))
heatmap(matrix, margins = c(10,10), col=(cm.colors(5)), xlab='Measures',
ylab='Organisms', main = 'Example heatmap')

# install the 'gplots' package
install.packages('gplots')

# Activate 'gplots' functions
library(gplots)
heatmap.2(matrix)
heatmap.2(matrix, margins = c(10,10))

heatmap.2(matrix, margins = c(10,10), col=(cm.colors(5)), xlab='Measures',
ylab='Organisms', main = 'Example heatmap')

heatmap.2(matrix, margins = c(10,10), col=c('red', 'blue', 'green', 'orange') ,
Colv = FALSE, dendrogram='row', xlab='Measures', ylab='Organisms',
main = 'Example heatmap', trace="none")

#4) What are the dimensions of the matrix we have just visualized, (<code>dim()</code>)? What are the dimensions of the <code>allFSA_counts</code>?


countPerOrganism <- aggregate(allFSA_counts, by=list(allFSA_counts$orgname),  FUN=mean)

positions_percentage <- grepl(names(countPerOrganism), pattern = 'percent')
columns_percentage <- names(countPerOrganism)[ positions_percentage ]
data_percentages <- countPerOrganism[, columns_percentage]

matrix_percentages <- data.matrix(data_percentages)

data_rownames <- countPerOrganism$Group.1
rownames(matrix_percentages) <- data_rownames

#2) Create a heatmap of <code>matrix_percentages</code>. Save the plot
#3) Remove the dendrogram and clustering on the rows, columns or both. Save the plot.

